import Head from 'next/head'
import React, { Component } from 'react';
import { Button, Table, Container, Row, Col } from 'react-bootstrap';
import Layout from "../components/Layout/Layout";
import BookmarkIcon from '@material-ui/icons/Bookmark';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Link from 'next/link'
import BalanceSheet from '../components/Tables/BalanceSheet'
import ProfitAndLoss from '../components/Tables/ProfitAndLoss'


class ProfileDetail extends React.Component {
  render() {
    return (
      <Layout>
        <section className="profile-main-section">
          <section className="profile-banner-section">
            <Container>
              {/* Row Start */}
              <Row>
                <Col lg="12" className="py-4">
                  <Row>
                    <Col lg="6" md="6">
                      <div className="banner-heading">
                        <h1>Yes Bank Ltd <span>YESBANK</span></h1>
                        <h2>14.35 <span className="high"><svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-up-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path d="M7.247 4.86l-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z" />
                        </svg> 0.20 (1.41%)</span></h2>
                        {/* <h2>14.35 <span className="low"><svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-down-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
                        </svg> 0.20 (1.41%)</span></h2> */}
                      </div>
                    </Col>
                    <Col lg="6" md="6">
                      <div className="banner-btns">
                        <Button className="save-alert" title="Save Alert"><NotificationsIcon /> Save Alert</Button>
                        <Button className="watchlist-btn" title="Add To Watchlist"><BookmarkIcon /> Add To Watchlist</Button>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col lg="12">
                  <div className="profile-ratio">
                    <ul>
                      <li>
                        <h5>PE Ratio</h5>
                        <span>-0.64</span>
                      </li>
                      <li>
                        <h5>PB Ratio</h5>
                        <span>0.81</span>
                      </li>
                      <li>
                        <h5>Dividend Yield</h5>
                        <span>0.11</span>
                      </li>
                      <li>
                        <h5>Sector PE</h5>
                        <span>24.54</span>
                      </li>
                      <li>
                        <h5>Sector PB</h5>
                        <span>2.12</span>
                      </li>
                      <li>
                        <h5>Sector Div Yld</h5>
                        <span>0.71%</span>
                      </li>
                    </ul>
                  </div>
                </Col>
                <Col lg="12">
                  <div className="profile-tabs  table-responsive">
                    <ul>
                      <li>
                        <Link href="#">
                          <a title="Yes Bank" className="active-tab">Yes Bank</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a title="Chart">Chart</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a title="Analysis">Analysis</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a title="Peers">Peers</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a title="Quarters">Quarters</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a title="Profile & Loss">Yes Bank</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a title="Balance Sheet">Balance Sheet</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a title="Cash Flow">Cash Flow</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="#">
                          <a title="Ratios">Ratios</a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </Container>
          </section>
          <section className="profile-content-section">
            {/* Overview */}
            <Container>
              <Row>
                <Col md="12">
                  <div className="overview-box">
                    <Row>
                      <Col md="12">
                        <h5 className="table-heading">Overview</h5>
                      </Col>
                      <Col md="6">
                        <ul className="overview-ul">
                          <li>
                            <Link href="#"><a>Market cap <span>33698.00 CR.</span></a></Link>
                          </li>
                          <li>
                            <Link href="#"><a>Book value <span>21.44</span></a></Link>
                          </li>
                          <li>
                            <Link href="#"><a>Stock P/E <span>0</span></a></Link>
                          </li>
                          <li>
                            <Link href="#"><a>Dividend Yield <span>7.45%</span></a></Link>
                          </li>
                        </ul>
                      </Col>
                      <Col md="6">
                        <ul className="overview-ul-right">
                          <li>
                            ROCE <span>6.68%</span>
                          </li>
                          <li className="active-row">
                            ROE <span>6.53%</span>
                          </li>
                          <li>
                            Sales Growth (3Y) <span>29.84%</span>
                          </li>
                          <li className="active-row">
                            Face Value (3Y) <span>2.00</span>
                          </li>
                        </ul>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </Container>
            {/* ProfitAndLoss */}
            <ProfitAndLoss />
            {/* BalanceSheet */}
            <BalanceSheet />
          </section>
        </section>
      </Layout >
    );
  }
}
export default ProfileDetail;