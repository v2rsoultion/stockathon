import React, { Component, Fragment } from 'react';

import Form from 'react-bootstrap/Form';
import { Typeahead } from 'react-bootstrap-typeahead';
import options from './data';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import axios from 'axios';

class Autocomplete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      search: '',
    };
  }


  handleSearchInput = e => {
    // this.setState({ search: e.target.value });
  };


  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/posts')
      // axios.get('https://jsonplaceholder.typicode.com/todos/1')
      .then(response => {
        var rawData = [];
        response.data.map((item, key) => {
          rawData.push({
            label: item.title
          });
          if (response.data.length - 1 == key) {
            this.setState({
              data: rawData
            })
          }
        });
        console.log(this.state.data);
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <Fragment>
        <Form.Group>
          <Typeahead
            clearButton
            id="autocomplete"
            // onChange={this.handleSearchInput}
            options={this.state.data}
            placeholder="Search"
            selected={this.state.search}
          />
        </Form.Group>
      </Fragment>
    );
  }
}

export default Autocomplete;