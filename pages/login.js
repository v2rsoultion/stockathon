import Head from 'next/head'
import React, { Component } from 'react';
import Layout from "../components/Layout/Layout";
import { Checkbox, TextField, Button, FormControlLabel } from '@material-ui/core';
import { Col, Container, Form, Row, } from 'react-bootstrap';
import Link from 'next/link'
import CloseIcon from '@material-ui/icons/Close';

class Login extends React.Component {
  render() {
    return (
      <section className="login-section">
        <Container>
          {/* Row Start */}
          <Row>
            <Col xs="12" lg="12" sm="12" md="12" className="text-center mb-5">
              <Link href="/">
                <a>
                  <img src="images/logo.svg" width="300" />
                </a>
              </Link>
            </Col>
            <Col xs="12" lg="12" sm="12" md="12">
              <div className="login-box text-center">
                <Link href="/">
                  <a title="Close" className="close-login"> <CloseIcon /></a>
                </Link>
                {/* <img src="images/logo-icon.svg" width="50" /> */}
                <h2>Log in to Stockathon</h2>
                <p>Find your next great investment, now easier & faster with Stockathon</p>
                <Link href="#">
                  <a title="Google"><img src="images/google.svg" width="44" /> Google</a>
                </Link>
                <Link href="#">
                  <a title="Facebook"><img src="images/facebook.svg" width="44" /> Facebook</a>
                </Link>
                <div className="open-acc-box">
                  <span>Don't have a account yet?</span>
                  <Link href="#">
                    <a title="Open now">Open now</a>
                  </Link>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    );
  }
}
export default Login;

{/* <Row className="text-center">
                <Col xs="12" lg="12" sm="12" md="12">
                  <div className="text-center mb-5">
                    <img src="images/logo.svg" width="300" />
                  </div>
                  <h2 className="mb-4">Sign In</h2>
                </Col>
                <Form className="m-auto">
                  <Col xs="12" lg="12" sm="12" md="12" className="login-input">
                    <TextField id="userName" label="Username" variant="outlined" size="small" />
                  </Col>
                  <Col xs="12" lg="12" sm="12" md="12" className="login-input">
                    <TextField
                      id="password"
                      label="Password"
                      type="password"
                      autoComplete="current-password"
                      variant="outlined"
                      size="small"
                    />
                  </Col>
                  <Col xs="12" lg="12" sm="12" md="12" className="login-input">
                  <FormControlLabel
                    control={
                      <Checkbox
                        name="check"
                        color="primary"
                      />
                    }
                    label="Keep me logged in"
                  />
                </Col>
                  <Col xs="12" lg="12" sm="12" md="12" className="my-4">
                    <Button variant="outlined" color="primary" className="w-100">
                      Log In
                       </Button>
                  </Col>
                </Form>
              </Row> */}