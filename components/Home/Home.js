import React from 'react'
import { Form, FormControl, Button } from 'react-bootstrap'
import SearchIcon from '@material-ui/icons/Search';
import Link from 'next/link'
import Autocomplete from '../Autocomplete/Autocomplete';
class Home extends React.Component {

  render() {
    return <React.Fragment>
      {/* Hompage Main Section */}
      <section className="main-section">
        <div className="container">
          {/* Start Row */}
          <div className="row">
            <div className="col-12">
              <div className="homepage-logo text-center">
                <img src="images/logo-white.svg" className="desktop-logo" />
                <img src="images/logo-icon.svg" className="mobile-logo" />
                <p>The best research tool for equity investors in India</p>
              </div>
            </div>
            <div className="col-12">
              <div className="homepage-search">
                <h1>Search a company</h1>
                <Form className="homepage-form">
                  {/* <FormControl type="text" placeholder="Search" /> */}
                  <Autocomplete />
                  <Button ><SearchIcon /></Button>
                </Form>
              </div>
              <div className="homepage-links">
                <Link href="/profile-detail"><a title="Avanti Feeds">Avanti Feeds</a></Link>
                <Link href="/profile-detail"><a title="Godawari Power">Godawari Power</a></Link>
                <Link href="/profile-detail"><a title="Bharat Rasayan">Bharat Rasayan</a></Link>
                <Link href="/profile-detail"><a title="Alembic Pharma">Alembic Pharma</a></Link>
                <Link href="/profile-detail"><a title="Cupid">Cupid</a></Link>
                <Link href="/profile-detail"><a title="Marksans Pharma">Marksans Pharma</a></Link>
                <Link href="/profile-detail"><a title="Poly Medicure">Poly Medicure</a></Link>
                <Link href="/profile-detail"><a title="Shivalik Bimetal">Shivalik Bimetal</a></Link>
                <Link href="/profile-detail"><a className="view-all-link" title="View All">View All</a></Link>
              </div>
              <div className="browse-companies">
                <p>Browse companies by themes and sectors</p>
              </div>
            </div>
          </div>
          {/* End Row */}
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <div className="homepage-news">
                <Link href="#"><a className="news-heading">News</a></Link>
                <div className="news-slider">
                  <marquee width="100%" >
                    <Link href="#"><a>Gainers & Losers: 10 stocks that moved the most on September 8... </a></Link>
                    <Link href="#"><a> Copper futures down 1.44% to Rs 521.90 per kg in evening trade...</a></Link>
                    <Link href="#"><a>Nasdaq slides 3% as tech rout deepens...</a></Link>
                  </marquee>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </React.Fragment>
  }
}

export default Home