import Head from 'next/head'
import React, { Component } from 'react';
import { Button, Table, Container, Row, Col } from 'react-bootstrap';
import Link from 'next/link'


class ProfitAndLoss extends React.Component {
  constructor() {
    super();

    this.state = {
      data: [
        { id: 1, name: "Revenue", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 2, name: "Cost of goods sold", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 3, name: "Gross Profit", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 4, name: "Gross Profit %", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 5, name: "Total Operating Expense", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 6, name: "EBIT", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 7, name: "EBIT %", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 8, name: "Interest Income", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 9, name: "Other Income", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 10, name: "Taxes", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 11, name: "Net Income", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "EBIT", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Interest Income", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
        { id: 12, name: "EPS", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Net Income", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Average Share Outstanding", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Taxes", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%" },
      ],
      addClass: false,
      expandedRows: [],
      colepseIds: []
    };
  }

  toggle = (id) => {
    var colIndex = this.state.colepseIds.indexOf(id);
    var colIds = this.state.colepseIds;
    if (this.state.colepseIds.indexOf(id) != -1) {
      colIds.splice(colIndex, 1);
    } else {
      colIds.push(id);
    }
    this.setState({ colepseIds: colIds });

  }

  handleRowClick(rowId) {
    const currentExpandedRows = this.state.expandedRows;
    const isRowCurrentlyExpanded = currentExpandedRows.includes(rowId);

    const newExpandedRows = isRowCurrentlyExpanded ?
      currentExpandedRows.filter(id => id !== rowId) :
      currentExpandedRows.concat(rowId);

    this.setState({ expandedRows: newExpandedRows });
  }

  renderItem(item) {
    let boxClass = [""];
    if (this.state.addClass) {
      boxClass.push('active-tr');
    }
    const clickCallback = () => {
      this.handleRowClick(item.id);
      this.toggle(item.id);
    };
    const itemRows = [
      <tr className={this.state.colepseIds.indexOf(item.id) != -1 ? 'active-tr' : ''} >
        <td onClick={clickCallback} key={"row-data-" + item.id} className="cursor">{item.name} <Button></Button></td>
        <td>{item.value1}</td>
        <td>{item.value2}</td>
        <td>{item.value3}</td>
        <td>{item.value4}</td>
        <td>{item.value5}</td>
        <td>{item.value6}</td>
        <td>{item.value7}</td>
        <td>{item.value8}</td>
        <td>{item.value9}</td>
      </tr >,
    ];

    if (this.state.expandedRows.includes(item.id)) {
      itemRows.push(
        <tbody className="inner-tbody">
          <tr key={"row-expanded-" + item.id}>
            <td>{item.innername1}</td>
            <td>{item.innervalue1}</td>
            <td>{item.innervalue2}</td>
            <td>{item.innervalue3}</td>
            <td>{item.innervalue4}</td>
            <td>{item.innervalue5}</td>
            <td>{item.innervalue6}</td>
            <td>{item.innervalue7}</td>
            <td>{item.innervalue8}</td>
            <td>{item.innervalue9}</td>
          </tr>
          <tr key={"row-expanded-" + item.id}>
            <td>{item.innername2}</td>
            <td>{item.innervalue10}</td>
            <td>{item.innervalue11}</td>
            <td>{item.innervalue12}</td>
            <td>{item.innervalue13}</td>
            <td>{item.innervalue14}</td>
            <td>{item.innervalue15}</td>
            <td>{item.innervalue16}</td>
            <td>{item.innervalue17}</td>
            <td>{item.innervalue18}</td>
          </tr>
          <tr key={"row-expanded-" + item.id} >
            <td>{item.innername3}</td>
            <td>{item.innervalue19}</td>
            <td>{item.innervalue20}</td>
            <td>{item.innervalue21}</td>
            <td>{item.innervalue22}</td>
            <td>{item.innervalue23}</td>
            <td>{item.innervalue24}</td>
            <td>{item.innervalue25}</td>
            <td>{item.innervalue26}</td>
            <td>{item.innervalue27}</td>
          </tr>
          <tr key={"row-expanded-" + item.id}>
            <td>{item.innername4}</td>
            <td>{item.innervalue28}</td>
            <td>{item.innervalue29}</td>
            <td>{item.innervalue30}</td>
            <td>{item.innervalue31}</td>
            <td>{item.innervalue32}</td>
            <td>{item.innervalue33}</td>
            <td>{item.innervalue34}</td>
            <td>{item.innervalue35}</td>
            <td>{item.innervalue36}</td>
          </tr>
        </tbody>
      );
    }

    return itemRows;
  }


  render() {
    let allItemRows = [];

    this.state.data.forEach(item => {
      const perItemRows = this.renderItem(item);
      allItemRows = allItemRows.concat(perItemRows);
    });

    return (
      <Container>
        <Row>
          <Col lg="12">
            <div className="table-box">
              <Row>
                <Col md="6">
                  <h5 className="table-heading">Profit & Loss</h5>
                  <p className="table-p">Consolidated Figures in Rs. Crores / <Link href="#"><a>View Standalone</a></Link> </p>
                </Col>
                <Col md="6">
                  <Button className="table-button" title="Product Segments">Product Segments</Button>
                </Col>
              </Row>
              <div>
                <Table className="table-responsive">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Mar 2013</th>
                      <th>Mar 2014</th>
                      <th>Mar 2015</th>
                      <th>Mar 2016</th>
                      <th>Mar 2017</th>
                      <th>Mar 2018</th>
                      <th>Mar 2019</th>
                      <th>Mar 2020</th>
                      <th>TTM</th>
                    </tr>
                  </thead>
                  {/* Extra TR with sum total Class */}
                  <tr className="sum-tr">
                    <td>Total Operating Expense</td>
                    <td>8,294</td>
                    <td>9,981</td>
                    <td>11,572</td>
                    <td>13,533</td>
                    <td>16,425</td>
                    <td>20,269</td>
                    <td>29,624</td>
                    <td>26,052</td>
                    <td>21,587</td>
                  </tr>

                  {allItemRows}
                </Table>
              </div>
            </div>
          </Col>
        </Row>
      </Container>

    );
  }
}
export default ProfitAndLoss;