import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.scss';
import axios from 'axios';
function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
