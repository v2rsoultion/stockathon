import Head from 'next/head'
import React, { Component } from 'react';
import { Button, Table, Container, Row, Col } from 'react-bootstrap';
import Link from 'next/link'

class BalanceSheet extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [
        { id: 1, name: "Total Assets", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Research & Development", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "SGA Expense", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Inventory ", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Other Current Assets", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Receivables ", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 2, name: "Current Assets", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Cash Short Term Investments", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "A/C Receivables", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Inventory ", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Other Current Assets", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Receivables ", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 3, name: "Intangibles Assets", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Goodwill ", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Other Intangibles Assets", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other Long Term Assets ", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Other Current Assets", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Receivables ", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 4, name: "LT Assets", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Note Receivable Long Term", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Long Term Investments", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other Long Term Assets ", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Other Current Assets", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Receivables ", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 5, name: "Property Plant Equipment", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Note Receivable Long Term", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Long Term Investments", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other Long Term Assets ", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Other Current Assets", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Receivables ", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 6, name: "Total Liabilities", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Note Receivable Long Term", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Long Term Investments", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Other Long Term Assets ", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Other Long Term Assets ", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Receivables ", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 7, name: "Total Equity", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Common Stock", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Other Equity", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Retained Earning", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Unrealized Profit Loss Security", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Miscellaneous", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 8, name: "Current Liabilities", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Accounts Payable", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Accrued Liability", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Current Portion Long Term Debt", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Short Term Debt", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Current Liabilities", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 9, name: "Total Long Term Debt", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Accounts Payable", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Accrued Liability", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Current Portion Long Term Debt", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Short Term Debt", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Current Liabilities", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 10, name: "Other Liabilities", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Accounts Payable", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Accrued Liability", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Current Portion Long Term Debt", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Short Term Debt", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Current Liabilities", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
        { id: 11, name: "Deferred Income Tax", value1: "8,294", value2: "9,981", value3: "11,572", value4: "13,533", value5: "16,425", value6: "20,269", value7: "29,624", value8: "26,052", value9: "21,587", innername1: "Accounts Payable", innervalue1: "0%", innervalue2: "0%", innervalue3: "0%", innervalue4: "0%", innervalue5: "0%", innervalue6: "0%", innervalue7: "0%", innervalue8: "0%", innervalue9: "0%", innername2: "Accrued Liability", innervalue10: "8%", innervalue11: "8%", innervalue12: "9%", innervalue13: "9%", innervalue14: "10%", innervalue15: "10%", innervalue16: "8%", innervalue17: "9%", innervalue18: "9%", innername3: "Current Portion Long Term Debt", innervalue19: "0%", innervalue20: "0%", innervalue21: "0%", innervalue22: "0%", innervalue23: "0%", innervalue24: "0%", innervalue25: "0%", innervalue26: "0%", innervalue27: "0%", innername4: "Short Term Debt", innervalue28: "8%", innervalue29: "8%", innervalue30: "9%", innervalue31: "9%", innervalue32: "10%", innervalue33: "10%", innervalue34: "8%", innervalue35: "9%", innervalue36: "9%", innername5: "Other Current Liabilities", innervalue37: "0%", innervalue38: "0%", innervalue39: "0%", innervalue40: "0%", innervalue41: "0%", innervalue42: "0%", innervalue43: "0%", innervalue44: "0%", innervalue45: "0%" },
      ],
      addClass: false,
      expandedRows: [],
      colepseIds: []
    };
  }

  toggle = (id) => {
    var colIndex = this.state.colepseIds.indexOf(id);
    var colIds = this.state.colepseIds;
    if (this.state.colepseIds.indexOf(id) != -1) {
      colIds.splice(colIndex, 1);
    } else {
      colIds.push(id);
    }
    this.setState({ colepseIds: colIds });

  }

  handleRowClick(rowId) {
    const currentExpandedRows = this.state.expandedRows;
    const isRowCurrentlyExpanded = currentExpandedRows.includes(rowId);

    const newExpandedRows = isRowCurrentlyExpanded ?
      currentExpandedRows.filter(id => id !== rowId) :
      currentExpandedRows.concat(rowId);

    this.setState({ expandedRows: newExpandedRows });
  }

  renderItem(item) {
    let boxClass = [""];
    if (this.state.addClass) {
      boxClass.push('active-tr');
    }
    const clickCallback = () => {
      this.handleRowClick(item.id);
      this.toggle(item.id);
    };
    const itemRows = [
      <tr className={this.state.colepseIds.indexOf(item.id) != -1 ? 'active-tr' : ''} >
        <td onClick={clickCallback} key={"row-data-" + item.id} className="cursor">{item.name} <Button></Button></td>
        <td>{item.value1}</td>
        <td>{item.value2}</td>
        <td>{item.value3}</td>
        <td>{item.value4}</td>
        <td>{item.value5}</td>
        <td>{item.value6}</td>
        <td>{item.value7}</td>
        <td>{item.value8}</td>
        <td>{item.value9}</td>
      </tr >,
    ];

    if (this.state.expandedRows.includes(item.id)) {
      itemRows.push(
        <tbody className="inner-tbody">
          <tr key={"row-expanded-" + item.id}>
            <td>{item.innername1}</td>
            <td>{item.innervalue1}</td>
            <td>{item.innervalue2}</td>
            <td>{item.innervalue3}</td>
            <td>{item.innervalue4}</td>
            <td>{item.innervalue5}</td>
            <td>{item.innervalue6}</td>
            <td>{item.innervalue7}</td>
            <td>{item.innervalue8}</td>
            <td>{item.innervalue9}</td>
          </tr>
          <tr key={"row-expanded-" + item.id}>
            <td>{item.innername2}</td>
            <td>{item.innervalue10}</td>
            <td>{item.innervalue11}</td>
            <td>{item.innervalue12}</td>
            <td>{item.innervalue13}</td>
            <td>{item.innervalue14}</td>
            <td>{item.innervalue15}</td>
            <td>{item.innervalue16}</td>
            <td>{item.innervalue17}</td>
            <td>{item.innervalue18}</td>
          </tr>
          <tr key={"row-expanded-" + item.id} >
            <td>{item.innername3}</td>
            <td>{item.innervalue19}</td>
            <td>{item.innervalue20}</td>
            <td>{item.innervalue21}</td>
            <td>{item.innervalue22}</td>
            <td>{item.innervalue23}</td>
            <td>{item.innervalue24}</td>
            <td>{item.innervalue25}</td>
            <td>{item.innervalue26}</td>
            <td>{item.innervalue27}</td>
          </tr>
          <tr key={"row-expanded-" + item.id}>
            <td>{item.innername4}</td>
            <td>{item.innervalue28}</td>
            <td>{item.innervalue29}</td>
            <td>{item.innervalue30}</td>
            <td>{item.innervalue31}</td>
            <td>{item.innervalue32}</td>
            <td>{item.innervalue33}</td>
            <td>{item.innervalue34}</td>
            <td>{item.innervalue35}</td>
            <td>{item.innervalue36}</td>
          </tr>
          <tr key={"row-expanded-" + item.id}>
            <td>{item.innername5}</td>
            <td>{item.innervalue28}</td>
            <td>{item.innervalue29}</td>
            <td>{item.innervalue30}</td>
            <td>{item.innervalue31}</td>
            <td>{item.innervalue32}</td>
            <td>{item.innervalue33}</td>
            <td>{item.innervalue34}</td>
            <td>{item.innervalue35}</td>
            <td>{item.innervalue36}</td>
          </tr>
        </tbody>
      );
    }
    return itemRows;
  }
  render() {
    let allItemRows = [];

    this.state.data.forEach(item => {
      const perItemRows = this.renderItem(item);
      allItemRows = allItemRows.concat(perItemRows);
    });

    return (
      <Container>
        <Row>
          <div className="col-lg-12">
            <div className="table-box">
              <Row>
                <div className="col-md-6">
                  <h5 className="table-heading">Balance Sheet</h5>
                  <p className="table-p">Consolidated Figures in Rs. Crores / <Link href="#"><a>View Standalone</a></Link> </p>
                </div>
                <div className="col-md-6">
                  <Button className="table-button" title="Corporate Actions">Corporate Actions</Button>
                </div>
              </Row>
              <div>
                <Table className="table-responsive">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Mar 2013</th>
                      <th>Mar 2014</th>
                      <th>Mar 2015</th>
                      <th>Mar 2016</th>
                      <th>Mar 2017</th>
                      <th>Mar 2018</th>
                      <th>Mar 2019</th>
                      <th>Mar 2020</th>
                      <th>TTM</th>
                    </tr>
                  </thead>
                  {allItemRows}
                </Table>
              </div>
            </div>
          </div>
        </Row>
      </Container>
    );
  }
}
export default BalanceSheet;