import React from 'react';
import { httpGet } from '../_helper/ApiBase';
var data = null;
export const CommonService = {
  getConfiguration
};

function getConfiguration(cb) {

  httpGet('getconfiguration', (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        }
        // globalConfiguration.next();
      } else {
        var res = {
          status: false,
          message: response.message
        }
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}
