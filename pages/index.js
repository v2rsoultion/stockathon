import Head from 'next/head'
import React, { Component } from 'react';
import Layout from "../components/Layout/Layout";
import Home from "../components/Home/Home"

class DefaultPage extends React.Component {
  render() {
    return (
      <Layout>
        <Home />
      </Layout>
    );
  }
}
export default DefaultPage;