import Head from 'next/head'
import React, { Component } from 'react';
import Layout from "../components/Layout/Layout";
import { Container, Col, Row } from 'react-bootstrap';
import Link from 'next/link'

class Premium extends React.Component {
  render() {

    let freePlan = [
      "Custom Stock Screens",
      "Watchlist of 50 Companies",
      "2 Screen Alerts",
      "15 Columns for comparison",
      "Customized Export to Excel",
      "Custom ratios"
    ]

    let premiumPlan = [
      "Segment Results",
      "Trade data of 9000+ products",
      "Upto 50 columns for comparison",
      "Follow unlimited companies",
      "Unlimited screen alerts*",
      "Download Watchlist / Screen results",
      "Detailed peer comparison",
      "Industry filter for screens",
      "Multiple Watchlists",
      "Notes to accounts",
      "Credit Rating reports",
      "Priority Support"
    ]

    return (
      <Layout className="inner-header">
        <section className="content-pages">
          <Container>
            <Row>
              <Col lg={12} className="text-center heading-classes">
                <h1>
                  Save hours spent in getting data
                </h1>
                <p>
                  Get unlimited access to all the features
                </p>
              </Col>
            </Row>

            <Row className="mt-4">
              <Col lg={1}></Col>
              <Col lg={10} xs={12} sm={12}>
                <Row className="wrapper">
                  <Col lg={6} xs={12} md={6} sm={6}>
                    <div className="plan">
                      <header>
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-bookmark-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                          <path fillRule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                        </svg>
                        <h4 className="plan-title">Hobby Investor</h4>
                        <div className="plan-cost"><span className="plan-price">₹0</span><span className="plan-type">/year</span></div>
                      </header>
                      <ul className="plan-features">
                        <h4>Includes</h4>
                        {
                          freePlan.map((planName, key) => {
                            return <li key={key}>
                              <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-check2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
                              </svg>
                              {planName}
                            </li>
                          }
                          )}
                      </ul>
                      <div className="plan-select">
                        <Link href="/login">
                          <a> Get a free account</a>
                        </Link>
                      </div>
                    </div>
                  </Col>
                  <Col lg={6} xs={12} md={6} sm={6}>
                    <div className="plan">
                      <header>
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-bookmark-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                          <path fillRule="evenodd" d="M6.146 5.146a.5.5 0 0 1 .708 0L8 6.293l1.146-1.147a.5.5 0 1 1 .708.708L8.707 7l1.147 1.146a.5.5 0 0 1-.708.708L8 7.707 6.854 8.854a.5.5 0 1 1-.708-.708L7.293 7 6.146 5.854a.5.5 0 0 1 0-.708z" />
                        </svg>
                        <h4 className="plan-title">Active Investor</h4>
                        <div className="plan-cost"><span className="plan-price">₹4,999</span><span className="plan-type">/year</span></div>
                      </header>
                      <ul className="plan-features">
                        <h4>Everything in free, plus:</h4>
                        {
                          premiumPlan.map((planName, key) => {
                            return <li key={key}>
                              <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-check2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
                              </svg>
                              {planName}
                            </li>
                          }
                          )}
                      </ul>
                      <div className="plan-select">
                        <Link href="/login">
                          <a>Login to Buy</a>
                        </Link>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col lg={1}></Col>
            </Row>

            <Row className="price-footer">
              <Col lg={12} className="text-center">
                <p>
                  *Unlimited access: Though there are internal-limits on few resource extensive features such  <br />as emails and columns, these limits are set pretty high.
                </p>
              </Col>
            </Row>
          </Container>
        </section>

        <section className="faq-section">
          <Container>
            <Row>
              <Col lg={12} className="text-center mb-4">
                <h5 className="table-heading">Frequently asked questions</h5>
                <p className="sub_heading">Questions that are commonly asked</p>
              </Col>
              <Col lg={6} >
                <div className="quetions_listing">
                  <h5>
                    <span>
                      1
                  </span>
                  How is premium useful?
                </h5>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the
                    1500s, when an unknown printer took a galley of type and scrambled it to
                    make a type specimen book. It has survived not only five centuries, but
                    also the leap into electronic typesetting, remaining essentially
                    unchanged. It was popularised in the 1960s with the release of Letraset
                    sheets containing Lorem Ipsum passages, and more recently with desktop
                    publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
                </div>
              </Col>
              <Col lg={6}>
                <div className="quetions_listing">
                  <h5> <span>
                    2
                  </span>How is premium useful?</h5>
                  <p>Premium subscription provides the complete package. It enables you to add more columns and companies. The trade data and segment results are helpful in quick analysis.</p>
                </div>
              </Col>
              <Col lg={6}>
                <div className="quetions_listing">
                  <h5> <span>
                    3
                  </span> How is premium useful?</h5>
                  <p>Premium subscription provides the complete package. It enables you to add more columns and companies. The trade data and segment results are helpful in quick analysis.</p>
                </div>
              </Col>
              <Col lg={6} >
                <div className="quetions_listing">
                  <h5> <span>
                    4
                  </span>How is premium useful?</h5>
                  <p>Premium subscription provides the complete package. It enables you to add more columns and companies. The trade data and segment results are helpful in quick analysis.</p>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </Layout>
    );
  }
}
export default Premium;