import React, { Component } from 'react';
import Head from 'next/head';
import { Form, FormControl, Nav, Navbar, NavDropdown, Button } from 'react-bootstrap';
import SearchIcon from '@material-ui/icons/Search';
import Link from 'next/link'
import Autocomplete from '../Autocomplete/Autocomplete';

class Layout extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <React.Fragment>
        < Head >
          <meta charSet="utf-8" />
          <title>Stockathon</title>
          <link
            rel="icon"
            href="favicon.ico"
            type="image/ico"
            sizes="16x16" />
          <meta
            name="viewport"
            content="width=device-width,  initial-scale=1 maximum-scale=1.0, user-scalable=no" />
        </Head >

        {/* Header */}
        <Navbar expand="lg" className="stockathon-nav">
          <Link href="/">
            <a className="navbar-brand">
              <img src="images/logo.svg" width="200" />
            </a>
          </Link>

          <Navbar.Toggle aria-controls="basic-navbar-nav" className="x" >
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav">
            <Form inline className="mr-auto header-form">
              {/* <FormControl type="text" placeholder="Search" className="header-search" /> */}
              <div className="header-search">
                <Autocomplete />
              </div>
              <Button className="header-search-btn"><SearchIcon /></Button>
            </Form>
            <Nav>
              <Link href="#">
                <a className="nav-link" title="Features">Features</a>
              </Link>
              <Link href="#">
                <a className="nav-link" title="Screens">Screens</a>
              </Link>
              <Link href="/premium">
                <a className="nav-link" title="Premium">Premium</a>
              </Link>
              <div className="header-btns">
                <Link href="/login">
                  <Button className="header-login-btn" title="Log In" >Log In</Button>
                </Link>
                <Button className="header-register-btn" title="Register">Register</Button>
              </div>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        {this.props.children}

        <footer>
          <section className="footer-section">
            <div className="container">
              {/* Start Row */}
              <div className="row">
                <div className="col-12">
                  <div className="footer-links">
                    <ul>
                      <li>
                        <Link href="#"><a title="Home">Home</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="About Us">About Us</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="Support">Support</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="Premium">Premium</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="What's New">What's New</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="Privacy">Privacy</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="Terms">Terms</a></Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="footer-copyright">
                  <p>Copyright &copy;2020 <span>Stockathon.</span> All Rights Reserved</p>
                </div>
              </div>
              {/* End Row */}
            </div>
          </section>
        </footer>
      </React.Fragment>
    );
  }
}

export default Layout;



